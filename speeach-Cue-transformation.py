#!/usr/bin/env python2.7
# script by Alex Eames http://RasPi.tv
# http://RasPi.tv/how-to-use-interrupts-with-python-on-the-raspberry-pi-and-rpi-gpio-part-3
import RPi.GPIO as GPIO
import os
import subprocess
import signal
import os.path
import sys
import re
import time
GPIO.setmode(GPIO.BCM)

# GPIO 23 & 17 set up as inputs, pulled up to avoid false detection.
# Both ports are wired to connect to GND on button press.
# So we'll be setting up falling edge detection for both
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# GPIO 24 set up as an input, pulled down, connected to 3V3 on button press
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

# now we'll define two threaded callback functions
# these will run in another thread when our events are detected
def my_callback(channel):
    print "Clearing the Image viewer process....!"
    p = subprocess.Popen(['pgrep', '-l' , 'feh'], stdout=subprocess.PIPE)
    out, err = p.communicate()

    for line in out.splitlines():        
    	line = bytes.decode(line)
    	pid = int(line.split(None, 1)[0])
    	os.kill(pid, signal.SIGKILL)
def my_callback2(channel):
    #print "My name:"
    image_file = "abcdef.jpg"
    #subprocess.call(['feh', 'abcdef.jpg'])   
    process1=subprocess.Popen(['feh','-g','840x640', '%s' %image_file]) 
#print "Make sure you have a button connected so that when pressed"
#print "it will connect GPIO port 23 (pin 16) to GND (pin 6)\n"
#print "You will also need a second button connected so that when pressed"
#print "it will connect GPIO port 24 (pin 18) to 3V3 (pin 1)\n"
#print "You will also need a third button connected so that when pressed"
#print "it will connect GPIO port 17 (pin 11) to GND (pin 14)"
#raw_input("Press Enter when ready\n>")
# when a falling edge is detected on port 17, regardless of whatever 
# else is happening in the program, the function my_callback will be run
GPIO.add_event_detect(17, GPIO.FALLING, callback=my_callback, bouncetime=300)

# when a falling edge is detected on port 23, regardless of whatever 
# else is happening in the program, the function my_callback2 will be run
# 'bouncetime=300' includes the bounce control written into interrupts2a.py
GPIO.add_event_detect(23, GPIO.FALLING, callback=my_callback2, bouncetime=300)

try:
    print "Waiting for the user input to start recording"
    print "Press your button when ready to initiate the process....."
    GPIO.wait_for_edge(24, GPIO.RISING)
    print "\nSystem is now ready to record the Voice. Kindly speak to record the voice...."
    p = subprocess.Popen(["speech-recog.sh", "-f cd -t wav -d 4"], stdout=subprocess.PIPE,stdin=subprocess.PIPE)
    out, err = p.communicate()
    print out
   
    print "Length of the string: ", len(out);
    newString=out[1:len(out)-1]
    newString=(newString.lower())

    VideoFile="Video/"+newString+".mp4"
    AudioFile="Audio/"+newString+".wav"
    ImageFile="Image/"+newString+".jpg"
    print "Video file name : ", VideoFile
    print "Audio file name : ", AudioFile
    print "Image file name : ", ImageFile
    if os.path.isfile(AudioFile) and os.access(AudioFile, os.R_OK):
        print "Congratulations, Speech recognised Correctly....!!"
        print "Playing Back recognised Audio....!!"
        subprocess.call(['aplay', AudioFile])
        print "************************************************"
        print "Displaying the Image.....!!"

        #subprocess.call(['feh', ImageFile])
        pp1=subprocess.Popen(['feh','-g','840x640', '%s' %ImageFile]) 

        time.sleep(8)
        pp1.kill()
        print "************************************************"
        print "Playing Back recognised Video....!!"
        #subprocess.call(['omxplayer', VideoFile])
	options="10 10 850 500" 
        subprocess.Popen(['omxplayer','--win', '%s' %options, '%s' %VideoFile])
        print "************************************************"
    else:
        print "Either file is missing or Speak clearely and retry....!!"

except KeyboardInterrupt:
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit
GPIO.cleanup()           # clean up GPIO on normal exit

